Gulliv'Air - Server
===

## Présentation du projet

Gulliv'Air est un projet générique de capteurs [Gulliv'Air](https://gitlab.com/GullivAir/Doc).

Server est la partie gestion des données envoyées par les capteurs et les requètes de visualisation.  
Nous allons essayer de respecter le standard [SoS](https://en.wikipedia.org/wiki/Sensor_Observation_Service).

## Documentation du projet

Vous pouvez trouver la documentation de ce projet dans le [Wiki](https://gitlab.com/GullivAir/Server/wikis/home).

### Contributions

Gulliv'Air est un projet libre et nous sommes très heureux d'accepter les contributions de la communauté. Veuillez vous référer à la page [Contributions](https://gitlab.com/GullivAir/Doc/wikis/contributions) pour plus de détails.

## Licence

Le projet est sous licence GPLv3. Pour plus d'informations, regarder les droits en visitant cette page [https://www.gnu.org/licenses/gpl-3.0.html](https://www.gnu.org/licenses/gpl-3.0.html).

